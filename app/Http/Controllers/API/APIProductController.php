<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class APIProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return response()->json(['data' => ProductResource::collection($products)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'store_id' => 'required',
            'photo' => 'required|mimes:png,jpg'
        ]);

        $name = request('name');
        $price = request('price');
        $description = request('description');

        $store_id = request('store_id');

        $photo = $request->file('photo');
        $fileName = "";
        $filePath = "";
        if ($photo) {
            $fileName = $photo->getClientOriginalName();
            // return response()->json($fileName);
            $filePath = $photo->storeAs('images/products', $fileName, 'public');
        }

        $product = Product::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'price' => $price,
            'description' => $description,
            'photo' => $filePath ?? "",
            'store_id' => $store_id
        ]);




        if ($product) {
            return response()->json(['Message' => "Berhasil Menambahkan Produk", 'Product' => new ProductResource($product)]);
        } else {
            return response()->json(['Message' => "Gagal Menambahkan Produk"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // return response()->json(['Message' => $request->all()]);
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            // 'photo' => 'required|mimes:png,jpg'
        ]);

        $product->name = $request->input('name');
        $product->slug = Str::slug($request->input('name'));
        $product->price = $request->input('price');
        $product->description = $request->input('description');

        $photo = $request->file('photo');
        $fileName = "";
        $filePath = "";
        if ($photo) {
            // error penghapusan
            Storage::delete('public/' . $product->photo);

            $fileName = time() . '_' . $photo->getClientOriginalName();
            $filePath = $photo->storeAs('images/products', $fileName, 'public');
        }

        $product->photo = $filePath ?? $product->photo;

        if ($product->save()) {
            return response()->json(['Message' => "Berhasil Mengubah Produk", 'Product' => new ProductResource($product)]);
        } else {
            return response()->json(['Message' => "Gagal Mengubah Produk"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if ($product->delete()) {
            return response()->json(['Message' => "Berhasil Menghapus Produk"]);
        } else {
            return response()->json(['Message' => "Gagal Menghapus Produk", 'Product' => new ProductResource($product)]);
        }
    }
}
