<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\StoreResource;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class APIStoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();
        return response()->json(['data' => StoreResource::collection($stores)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string|max:255',
            'user_id'      => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $user_has_store = Store::where('user_id', request('user_id'))->get();
        if (count($user_has_store) == 0) {
            $store = Store::create([
                'name' => request('name'),
                'user_id' => request('user_id')
            ]);

            if ($store) {
                return response()->json(['Message' => "Berhasil Menambahkan Toko", 'Store' => new StoreResource($store)]);
            } else {
                return response()->json(['Message' => "Gagal Menambahkan Toko"]);
            }
        } else {
            return response()->json(['msg' => "User Sudah Memiliki Toko"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        // return response()->json(request('name'));
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $store->name = request('name');

        if ($store->save()) {
            return response()->json(['Message' => "Berhasil Mengubah Toko", 'Store' => new StoreResource($store)]);
        } else {
            return response()->json(['Message' => "Gagal Mengubah Toko"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        if ($store->delete()) {
            return response()->json(['Message' => "Berhasil Menghapus Toko"]);
        } else {
            return response()->json(['Message' => "Gagal Menghapus Toko", 'Store' => new StoreResource($store)]);
        }
    }
}
