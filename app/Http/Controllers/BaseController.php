<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Review;
use App\Models\Store;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;

class BaseController extends Controller
{

    public function index()
    {
        $products = Product::all();

        if (request()->ajax()) {
            return DataTables::of($products)
                ->addIndexColumn()
                ->addColumn('store.name', function ($item) {
                    return $item->store->name;
                })
                ->addColumn('action', function ($item) {
                    $render = '
                    <a href="' . route('product.edit', $item->id) . '" class="btn btn-success">Edit</a>
                    <a id="delete-item" item-id="' . $item->id . '" class="btn btn-danger btn-delete" >Hapus</a>';

                    return $render;
                })->addColumn('photo', function ($item) {
                    $url = asset('storage/' . $item->photo);
                    return '<img src="' . $url . '" alt="photo" heigh="50" width="50"/>';
                })->rawColumns(['photo', 'action'])
                ->make(true);
        }
        // return dd($products);
        return view('pages.products.index');
    }

    public function users(Request $request)
    {
        $users = User::all();
        if ($request->ajax()) {
            return DataTables::of($users)
                ->addIndexColumn()
                ->addColumn('action', function ($item) {
                    $render =
                        '<a href="/user/' . $item->id . '?key=adsbexkm2022" class="btn btn-success">Detail</a>';

                    return $render;
                })
                ->make(true);
        }

        return view('pages.users.index');
    }

    public function detail($id)
    {
        $user = User::find($id);
        return view('pages.users.detail', compact('user'));
    }
}
