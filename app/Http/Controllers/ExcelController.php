<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use App\Imports\UsersImport;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Exception;
use App\Traits\RefreshDatabaseWithData;
use Yajra\DataTables\Facades\DataTables;

class ExcelController extends Controller
{

    public function index()
    {
        $data = User::get();

        if (request()->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->make(true);
        }

        return view('pages.excel.excel');
    }

    public function import(Request $request)
    {

        // $file = $request->file('file');
        // Excel::import(new UsersImport, $file);
        // return redirect()->back()->with(['success' => 'Berhasil Import']);
        $validate = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->with(['error' => $validate]);
        } else {
            try {
                $file = $request->file('file');
                $file_name = 'user' . \Carbon\Carbon::now()->isoFormat('D-M-YY-hh-mm-ss-') . $file->getClientOriginalName();
                $file_path = 'imports/user';

                $is_imported = Excel::import(new UsersImport, $file);
                $file->move($file_path, $file_name);

                if ($is_imported) {
                    return redirect()->back()->with(['success' => 'Berhasil Import']);
                } else {
                    return redirect()->back()->with(['error' => 'Gagal Import']);
                }
            } catch (Exception $th) {
                return redirect()->back()->withErrors($th->getMessage());
            }
        }
        return redirect()->back()
            ->withInput()
            ->withErrors($validate)
            ->with(['error' => 'Data Invalid']);
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
