<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Store;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->action([BaseController::class, 'index']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores = Store::all();
        return view('pages.products.add', compact('stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'store' => 'required',
            'photo' => 'required|mimes:png,jpg'
        ]);

        $name = $request->input('name');
        $price = $request->input('price');
        $description = $request->input('description');

        $store_id = $request->input('store');
        $photo = $request->file('photo')->store('uploads');

        $product = Product::create([
            'name' => $name,
            'slug' => Str::slug($name),
            'price' => $price,
            'description' => $description,
            'photo' => $photo,
            'store_id' => $store_id
        ]);




        if ($product) {
            return redirect('/')->with('success', 'Sukses Menambah Produk');
        } else {
            return redirect('/')->with('fail', 'Sukses Menambah Produk');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $stores = Store::all();
        return view('pages.products.edit', ['product' => $product, 'stores' => $stores]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'store' => 'required',
            'photo' => 'required|mimes:png,jpg'
        ]);

        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->description = $request->input('description');
        $product->store_id = $request->input('store');


        if (is_file($product->photo)) {
            // 1. possibility
            Storage::delete($product->photo);
            // 2. possibility
            unlink(storage_path('app/uploads/' . $product->photo));
        }
        $product->photo = $request->file('photo')->store('uploads');
        if ($product->save()) {
            return redirect('/')->with('success', 'Sukses Mengubah Produk');
        } else {
            return redirect('/')->with('fail', 'Gagal Mengubah Produk');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            if (request()->ajax()) {
                $is_deleted = $product->delete();
                return response()->json([
                    'msg' => $is_deleted
                ], 200);
            } else {
                if ($product->delete()) {
                    return redirect('/')->with('success', 'Sukses Menghapus Produk');
                } else {
                    return redirect('/')->with('fail', 'Gagal Menghapus Produk');
                }
            }
        } catch (Exception $th) {
            return redirect('/')->with('fail', 'Error Server');
        }
    }
}
