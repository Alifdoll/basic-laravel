<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductReview;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return dd(auth()->user()->id);
        $reviews = ProductReview::where('user_id', auth()->user()->id)->get();
        // return dd($reviews[0]->product);
        return view('pages.reviews.index', ['reviews' => $reviews]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::find(auth()->user()->id);
        $products = Product::all();
        return view('pages.reviews.add', compact('users', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'score' => 'required',
            'review' => 'required',
            'product_id' => 'required',
            'user_id' => 'required',
        ]);

        $score = $request->input('score');
        $review = $request->input('review');
        $user_id = $request->input('user_id');
        $product_id = $request->input('product_id');

        // return dd("TEST");
        $product = Product::find($product_id);
        $product->reviews()->attach($user_id, [
            'score' => $score,
            'review' => $review
        ]);



        if ($product->save()) {
            return redirect('/review')->with('success', 'Sukses Menambah Komentar');
        } else {
            return redirect('/review')->with('fail', 'Gagal Menambah Komentar');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductReview $review)
    {
        $user = Auth::user();
        return view('pages.reviews.edit', ['review' => $review, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductReview $review)
    {
        $request->validate([
            'score' => 'required',
            'review' => 'required',
        ]);

        $review->review = $request->input('review');
        $review->score = $request->input('score');

        if ($review->save()) {
            return redirect('/review')->with('success', 'Sukses Mengubah Review');
        } else {
            return redirect('/review')->with('fail', 'Gagal Mengubah Review');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
