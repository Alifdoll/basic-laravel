<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();
        if (request()->ajax()) {
            return DataTables::of($stores)
                ->addIndexColumn()
                ->addColumn('store.owner', function ($item) {
                    return $item->user->name;
                })
                ->addColumn('action', function ($item) {
                    $render = '
                    <a href="' . route('store.edit', $item->id) . '" class="btn btn-success">Edit</a>
                    <a id="delete-store" item-id="' . $item->id . '" class="btn btn-danger btn-delete" >Hapus</a>';

                    return $render;
                })
                ->make(true);
        }
        return view('pages.stores.index', compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::doesntHave('store')->get();
        if (count($users) > 0) {
            return view('pages.stores.add', compact('users'));
        } else {
            return redirect('/store')->with('fail', 'Semua user sudah memiliki toko');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required',
                'user_id' => 'required'
            ]);

            $name = $request->input('name');
            $user_id = $request->input('user_id');

            $store = Store::create([
                'name' => $name,
                'user_id' => $user_id
            ]);

            if ($store) {
                return redirect('/store')->with('success', 'Sukses Menambah Toko');
            } else {
                return redirect('/store')->with('fail', 'Gagal Menambah Toko');
            }
        } catch (Exception $th) {
            return redirect('/store')->with('fail', 'Error Server atau user sudah memiliki toko');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        $users = User::doesntHave('store')->get();
        $store = $store;
        return view('pages.stores.edit', [
            'users' => $users,
            'store' => $store
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $store->name = $request->input('name');
        $store->user_id = $request->input('user_id') == null ? $store->user_id : $request->input('user_id');

        if ($store->save()) {
            return redirect('/store')->with('success', 'Sukses Mengubah Toko');
        } else {
            return redirect('/store')->with('fail', 'Gagal Mengubah Toko');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        try {
            if (request()->ajax()) {
                $is_deleted = false;
                $msg = $is_deleted ? "Sukses Menghapus Toko" : "Gagal Menghapus Toko";

                return response()->json([
                    'success' => $is_deleted,
                    'msg' => $msg
                ], 200);
            } else {
                if ($store->delete()) {
                    return redirect('/store')->with('success', 'Sukses Menghapus Toko');
                } else {
                    return redirect('/store')->with('fail', 'Gagal Menghapus Toko');
                }
            }
        } catch (Exception $th) {
            return redirect('/store')->with('fail', 'Error Server');
        }
    }
}
