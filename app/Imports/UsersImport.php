<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        DB::table('users')->delete();
        foreach ($rows as $row) {
            User::create([
                'name' => $row['name'],
                'username' => $row['username'],
                'phone_number' => $row['phone_number'],
                'email' => $row['email'],
                'city' => $row['city'],
                'password' => Hash::make('password'),
            ]);
        }
    }
}
