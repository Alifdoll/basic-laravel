<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'slug',
        'price',
        'description',
        'photo',
        'store_id',
    ];

    public function reviews()
    {
        return $this->belongsToMany(User::class, 'product_reviews')
            ->using(RoleUser::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
