<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductReview extends Pivot
{
    protected $table = 'product_reviews';

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
