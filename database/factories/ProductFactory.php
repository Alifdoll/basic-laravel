<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Store;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->text(20);
        $slug = Str::slug($name);
        return [
            'name' => $name,
            'slug' => $slug,
            'description' => $this->faker->text(50),
            'photo' => 'Photo.png',
            'store_id' => $this->faker->numberBetween(1, Store::count()),
            'price' => 10000.0
        ];
    }
}
