<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Review;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReviewFactory extends Factory
{

    protected $model = Review::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'score' => rand(1, 6),
            'review' => $this->faker->text(15),
            'product_id' => $this->faker->numberBetween(1, Product::count()),
            'user_id' => $this->faker->numberBetween(1, User::count())
        ];
    }
}
