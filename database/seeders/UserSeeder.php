<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->count(5)->create();
        $faker = Factory::create();
        User::factory()->create([
            'name' => 'alif',
            'username' => 'alif',
            'email' => 'alifianfajarpratama@gmail.com',
            'password' => Hash::make('password'),
            'city' => $faker->city(),
            'phone_number' => '6282334975575'
        ]);

        User::factory()->create([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin'),
            'city' => $faker->city(),
            'phone_number' => '6282334975575'
        ]);
    }
}
