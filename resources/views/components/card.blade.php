<div class="card">
    <div class="card-body">
        <a href="{{ $link }}">
            <h5 class="card-title">
                {{ $title ?? '' }}
            </h5>
        </a>

        <p class="card-text">
            {{ $slot ?? '' }}
        </p>
        <a class="btn btn-link" href="{{ $edit }}"><i class="bi bi-pen"></i></a>
        {{-- <form method="POST" action="{{ $delete }}">
            @csrf
            @method('DELETE')
            <button class="btn btn-link" type="submit"><i class="bi bi-trash"></i></button>
        </form> --}}
    </div>

</div>
