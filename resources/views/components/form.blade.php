@props([
    'action' => '',
    'id' => '',
])

<div>
    <form method="POST" action="{{ $action }}" id="{{ $id }}" enctype="multipart/form-data">
        @csrf
        {{ $slot }}
    </form>
</div>
