<nav>
    <ul>
        @if (Auth::check())
            <li><a href="/">Home</a></li>
            <li><a href="/store">Stores</a></li>
            <li><a href="/review">Product Review</a></li>
            <li><a href="/user">List User</a></li>
            <li>
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit">Logout</button>
                </form>
            </li>
        @else
            <li><a href="/excel">Excel</a></li>
        @endif

    </ul>
</nav>
