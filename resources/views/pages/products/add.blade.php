<x-app-layout title="Add Product">
    <x-form action="{{ route('product.store') }}" id="product-form">

        <div class="form-group">
            <label for="name">Name : </label>
            <input type="text" class="form-control" id="name" value="{{ old('name') }}" name="name">
            @error('name')
                <div class="alert alert-danger" role="alert">
                    Nama wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="product-price">Price : </label>
            <input type="number" class="form-control" id="product-price" value="{{ old('price') }}" name="price">
            @error('price')
                <div class="alert alert-danger" role="alert">
                    Harga wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="description">Description : </label>
            <textarea id="description" class="form-control" name="description" form="product-form">{{ old('description') }}</textarea>
            @error('description')
                <div class="alert alert-danger" role="alert">
                    Deskripsi wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="photo">Upload Photo</label>
            <input type="file" class="form-control-file" id="photo" value="{{ old('') }}" name="photo">
        </div>

        <div class="form-group">
            <label for="store">Store : </label>
            <select id="store" value="{{ old('') }}" name="store">
                @foreach ($stores as $store)
                    <option value="{{ $store->id }}">{{ $store->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Add Product</button>
    </x-form>
    @if ($errors->any())
        <h1>{{ $errors }}</h1>
    @endif
</x-app-layout>
