<x-app-layout title="Edit Product">
    <x-form action="{{ route('product.update', $product->id) }}" id="product-form">
        @method('PUT')
        <div class="form-group">
            <label for="name">Name : </label>
            <input type="text" class="form-control" id="name" value="{{ $product->name }}" name="name">
            @error('name')
                <div class="alert alert-danger" role="alert">
                    Nama wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="product-price">Price : </label>
            <input type="number" class="form-control" id="product-price" value="{{ $product->price }}" name="price">
            @error('price')
                <div class="alert alert-danger" role="alert">
                    Harga wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="description">Description : </label>
            <textarea id="description" class="form-control" name="description" form="product-form">{{ $product->description }}</textarea>
            @error('description')
                <div class="alert alert-danger" role="alert">
                    Deskripsi wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="photo">Upload Photo</label>
            <input type="file" class="form-control-file" id="photo" value="{{ $product->photo }}" name="photo">
        </div>

        <div class="form-group">
            <label for="store">Store : </label>
            <select id="store" name="store">
                @foreach ($stores as $store)
                    <option {{ $store->id == $product->store_id ? 'selected' : '' }} value="{{ $store->id }}">
                        {{ $store->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Edit Product</button>

    </x-form>
</x-app-layout>
