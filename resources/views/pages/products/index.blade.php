<x-app-layout title="Products">
    <div class="content">
        <h1>List of Products</h1>
    </div>

    <a class="btn btn-primary" href="product/create" role="button">Add Product</a>

    <div class="container">
        <div class="d-flex justify-content-center p-5">
            <table id="table-product" class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Photo</th>
                        <th>Nama Produk</th>
                        <th>Nama Toko</th>
                        <th>Harga Barang</th>
                        <th>Deskripsi Barang</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>



    @push('scripts')
        <script>
            $(document).ready(function() {
                var datatable;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $(function() {
                    datatable = $('#table-product').DataTable({
                        processing: true,
                        serverSide: true,
                        order: [
                            [0, 'asc']
                        ],
                        pageLength: 5,
                        lengthMenu: [2, 10, 50, 100],
                        pagingType: "simple",
                        ajax: "{{ url()->current() }}",
                        columns: [{
                                data: 'DT_RowIndex'
                            },
                            {
                                data: 'photo',
                            },
                            {
                                data: 'name'
                            },
                            {
                                data: 'store.name'
                            },
                            {
                                data: 'price'
                            },
                            {
                                data: 'description'
                            },
                            {
                                data: 'action'
                            },
                        ],
                    });
                });


            });

            $("#table-product").on("click", '.btn-delete', function() {
                var item_id = $(this).attr('item-id');
                var test = `{{ route('product.destroy', '') }}/${item_id}`;
                $.ajax({
                    type: 'DELETE',
                    url: `{{ route('product.destroy', '') }}/${item_id}`,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        console.log(data);
                        $("#table-product").DataTable().ajax.reload();
                    }

                })
            });
        </script>
    @endpush


</x-app-layout>
