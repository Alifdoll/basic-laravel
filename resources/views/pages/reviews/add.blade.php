<x-app-layout title="Add Review">
    <x-form action="{{ route('review.store') }}" id="review-form">

        <div class="form-group">
            <label for="review">Review : </label>
            <input type="text" class="form-control" id="review" value="{{ old('review') }}" name="review">
            @error('review')
                <div class="alert alert-danger" role="alert">
                    Review wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="score">Score : </label>
            <input type="number" class="form-control" id="score" min="0" max="5"
                value="{{ old('name') ? old('name') : 0 }}" name="score">
            @error('score')
                <div class="alert alert-danger" role="alert">
                    score wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="product_id">Product : </label>
            <select id="product_id" name="product_id">
                @foreach ($products as $product)
                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="user_id">Review By : </label>
            <select id="user_id" name="user_id" disabled>
                <option value="{{ $users->id }}">{{ $users->name }}</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Add Review</button>
    </x-form>
</x-app-layout>
