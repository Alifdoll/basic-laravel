<x-app-layout title="Add Review">
    <x-form action="{{ route('review.update', $review->id) }}" id="review-form">
        @method('PATCH')
        <div class="form-group">
            <label for="review">Review : </label>
            <input type="text" class="form-control" id="review" value="{{ $review->review }}" name="review">
            @error('review')
                <div class="alert alert-danger" role="alert">
                    Review wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="score">Score : </label>
            <input type="number" class="form-control" id="score" min="0" max="5"
                value="{{ $review->score }}" name="score">
            @error('score')
                <div class="alert alert-danger" role="alert">
                    score wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="product_id">Product : </label>
            <select id="product_id" name="product_id" disabled>
                <option value="{{ $review->product->id }}">{{ $review->product->name }}</option>
            </select>
        </div>

        <div class="form-group">
            <label for="user_id">Review By : </label>
            <select id="user_id" name="user_id" disabled>
                <option value="{{ $user->id }}">{{ $user->name }}</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Edit Review</button>
    </x-form>
</x-app-layout>
