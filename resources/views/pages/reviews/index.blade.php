<x-app-layout title="Reviews" show_edit="true" show_delet="true">
    <div class="content">
        <h1>List of Product Reviews by {{ Auth::user()->name }}</h1>
    </div>

    <a class="btn btn-primary" href="{{ route('review.create') }}" role="button">Add Review</a>


    @foreach ($reviews as $review)
        <x-card title="Produk : {{ $review->product->name }}" edit="{{ route('review.edit', $review->id) }}"
            delete="{{ route('review.destroy', $review->id) }}" link="#">
            <p class="card-text">
                "{{ $review->review }}", Score : {{ $review->score }}
            </p>
        </x-card>
    @endforeach
</x-app-layout>
