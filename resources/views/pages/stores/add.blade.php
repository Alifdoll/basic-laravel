<x-app-layout title="Add Product">
    <x-form action="{{ route('store.store') }}" id="product-form">

        <div class="form-group">
            <label for="name">Nama Toko : </label>
            <input type="text" class="form-control" id="name" value="{{ old('name') }}" name="name">
            @error('name')
                <div class="alert alert-danger" role="alert">
                    Nama wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="user_id">Owned By : </label>
            <select id="user_id" name="user_id">
                @foreach ($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Add Store</button>
    </x-form>
</x-app-layout>
