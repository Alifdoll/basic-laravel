<x-app-layout title="Add Product">
    <x-form action="{{ route('store.update', $store->id) }}" id="product-form">
        @method('PUT')
        <div class="form-group">
            <label for="name">Nama Toko : </label>
            <input type="text" class="form-control" id="name" value="{{ $store->name }}" name="name">
            @error('name')
                <div class="alert alert-danger" role="alert">
                    Nama wajib diisi
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="user_id">Owned By : </label>
            <select id="user_id" value="{{ old('') }}" name="user_id">
                @foreach ($users as $user)
                    <option {{ $user->id == $store->user_id ? 'selected' : '' }} value="{{ $user->id }}">
                        {{ $user->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Edit Store</button>
    </x-form>
</x-app-layout>
