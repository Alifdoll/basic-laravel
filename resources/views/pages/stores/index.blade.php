<x-app-layout title="Stores">
    <div class="content">
        <h1>List of Stores</h1>
    </div>

    <a class="btn btn-primary" href="store/create" role="button">Add Store</a>

    <div class="container">
        <div class="d-flex justify-content-center p-5">
            <table id="table-store" class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Toko</th>
                        <th>Nama Pemilik Toko</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    @push('scripts')
        <script>
            $(document).ready(function() {
                var datatable;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $(function() {
                    datatable = $('#table-store').DataTable({
                        processing: true,
                        serverSide: true,
                        order: [
                            [1, 'desc']
                        ],
                        pageLength: 5,
                        lengthMenu: [2, 10, 50, 100],
                        pagingType: "simple",
                        ajax: "{{ url()->current() }}",
                        columns: [{
                                data: 'DT_RowIndex'
                            },
                            {
                                data: 'name'
                            },
                            {
                                data: 'store.owner'
                            },
                            {
                                data: 'action'
                            },
                        ],
                    });
                });
            });

            $("#table-store").on("click", '.btn-delete', function() {
                var item_id = $(this).attr('item-id');
                $.ajax({
                    type: 'DELETE',
                    url: `{{ route('store.destroy', '') }}/${item_id}`,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        if (data.success) {
                            alert(data.success);
                            // $("#table-store").DataTable().ajax.reload();
                        } else {
                            alert(data.msg);
                        }
                        console.log(data);
                    }

                })
            });
        </script>
    @endpush

</x-app-layout>
