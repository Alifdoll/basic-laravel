<x-app-layout title="users">
    <div class="container">
        <div class="d-flex justify-content-center p-5">
            <table id="table-users" class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>City</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    @push('scripts')
        <script>
            $(document).ready(function() {
                var datatable;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $(function() {
                    datatable = $('#table-users').DataTable({
                        processing: true,
                        serverSide: true,
                        order: [
                            [0, 'asc']
                        ],
                        pageLength: 5,
                        lengthMenu: [2, 10, 50, 100],
                        pagingType: "simple",
                        ajax: "{{ url()->current() }}",
                        columns: [{
                                data: 'DT_RowIndex'
                            },
                            {
                                data: 'name'
                            },
                            {
                                data: 'email'
                            },
                            {
                                data: 'city'
                            },
                            {
                                data: 'action'
                            },

                        ],
                    });
                });


            });

            $("#table-users").on("click", '.btn-delete', function() {
                var item_id = $(this).attr('item-id');
                var test = `{{ route('product.destroy', '') }}/${item_id}`;
                $.ajax({
                    type: 'DELETE',
                    url: `{{ route('product.destroy', '') }}/${item_id}`,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        console.log(data);
                        $("#table-users").DataTable().ajax.reload();
                    }

                })
            });
        </script>
    @endpush
</x-app-layout>
