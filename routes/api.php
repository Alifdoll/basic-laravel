<?php

use App\Http\Controllers\API\APIProductController;
use App\Http\Controllers\API\APIStoreController;
use App\Http\Controllers\API\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('json.response')->group(function () {
    Route::controller(AuthController::class)->group(function () {
        Route::post('/register', 'register');
        // Route::get('/test', 'test');
        Route::post('/login', 'login');

        Route::middleware('auth:sanctum')->group(function () {
            // Route::get('/test', 'test');
            Route::prefix('/profile')->group(function () {
                Route::get('/', 'profile');
                Route::post('/', 'update');
            });

            Route::post('/logout', 'logout');
        });
    });
});

Route::middleware('json.response')->group(function () {
    Route::controller(APIStoreController::class)->group(function () {
        // Route::middleware('auth:sanctum')->gro
        Route::resource('/store', APIStoreController::class);
    });

    Route::resource('/product', APIProductController::class);
});
