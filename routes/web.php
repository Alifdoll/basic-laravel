<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\ExcelController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\StoreController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/', [BaseController::class, 'index'])->name('home');
    Route::resource('/product', ProductController::class);

    Route::resource('/store', StoreController::class);
    Route::resource('/review', ReviewController::class);



    Route::get('user/', [BaseController::class, 'users']);
    Route::get('user/{id}', [BaseController::class, 'detail'])->middleware('key');
});
Route::get('/excel', [ExcelController::class, 'index']);
Route::post('/import/user', [ExcelController::class, 'import'])->name('import.user');
Route::get('/export/user', [ExcelController::class, 'export'])->name('export.user');
// Route::get('/stores', [BaseController::class, 'stores']);
// Route::get('/reviews', [BaseController::class, 'reviews']);

// Route::get('user/', [BaseController::class, 'users']);
// Route::get('user/{id}', [BaseController::class, 'users'])->middleware('key');
